<?php

use PHPUnit\Framework\TestCase;

class DataManipulatorTest extends TestCase
{
    public function testAdd() {
        $dataManipulator = new DataManipulator([1, 2, 3]);
        $dataManipulator->add(4);

        $this->assertEquals([1, 2, 3, 4], $dataManipulator->getData());
    }

    public function testRemove() {
        $dataManipulator = new DataManipulator([1, 2, 3]);
        $dataManipulator->remove(1);

        $this->assertEquals([1, 3], $dataManipulator->getData());
    }

    public function testSum() {
        $dataManipulator = new DataManipulator([1, 2, 3, 4]);

        $this->assertEquals(10, $dataManipulator->sum());
    }

    public function testAverage() {
        $dataManipulator = new DataManipulator([1, 2, 3, 4, 5]);

        $this->assertEquals(3, $dataManipulator->average());
    }

    public function testReverse() {
        $dataManipulator = new DataManipulator([1, 2, 3, 4, 5]);
        $dataManipulator->reverse();

        $this->assertEquals([5, 4, 3, 2, 1], $dataManipulator->getData());
    }

    public function testFilterEven() {
        $dataManipulator = new DataManipulator([1, 2, 3, 4, 5, 6]);
        $dataManipulator->filterEven();

        $this->assertEquals([2, 4, 6], $dataManipulator->getData());
    }
}
