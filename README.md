![coverage](https://gitlab.com/fadil.xcoder/gitlab-ci-udemy/badges/master/coverage.svg?job=unit-testing-website)

# Usage

- Create a folder name for project `jigsaw`
- GOTO to the created folder and run command below,
- `composer require tightenco/jigsaw`
- Then, `./vendor/bin/jigsaw init`, this cmd is run once.
- To build app, `./vendor/bin/jigsaw build`
- `build_local` folder will be created with all static page inside it.
- To start server, `./vendor/bin/jigsaw serve`

```bash
./vendor/bin/jigsaw build && ./vendor/bin/jigsaw serve
```

# Test

- `vendor/bin/phpunit --do-not-cache-result --log-junit phpunit-report.xml --coverage-cobertura phpunit-coverage.xml --coverage-text --colors=never`

```bash
$  vendor/bin/phpunit --do-not-cache-result --log-junit phpunit-report.xml --coverage-cobertura phpunit-coverage.xml --coverage-text --colors=never
PHPUnit 10.4.1 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.1.11 with Xdebug 3.1.3
Configuration: C:\wamp64\www\jigsaw-gitlab-ci-cd\phpunit.xml

......                                                              6 / 6 (100%)

Time: 00:00.113, Memory: 10.00 MB

OK (6 tests, 6 assertions)

Generating code coverage report in Cobertura XML format ... done [00:00.003]


Code Coverage Report:    
  2023-10-29 09:34:33    

 Summary:
  Classes:  0.00% (0/1)  
  Methods: 87.50% (7/8)  
  Lines:   92.31% (12/13)

DataManipulator
  Methods:  87.50% ( 7/ 8)   Lines:  92.31% ( 12/ 13)

````

## URL

- http://localhost:8000/
- http://localhost:8000/en/post/how-to-raise-a-happy-kitten
- http://localhost:8000/fr/post/comment-bien-elever-votre-chaton