@extends('_layouts.main')

@section('body')
    <h1>{{ $page->title }}</h1>
    <p><b>{{ $page->author }}</b> - {{ date('F j, Y', $page->date) }}</p>
    @yield('content')
    <a href="{{ $page->baseUrl }}/" role="button">	&crarr;</a>
@endsection