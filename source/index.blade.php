@extends('_layouts.main')

@section('body')
<article class="grid">
    <div>
        <hgroup>
            <h1>{{ $page->description }}</h1>
            <h2>A minimalist layout for authentication by {{ $page->contact_email }}</h2>
        </hgroup>
        <form>
            <input
                type="text"
                name="login"
                placeholder="Login"
                aria-label="Login"
                autocomplete="nickname"
                required
            />
            <input
                type="password"
                name="password"
                placeholder="Password"
                aria-label="Password"
                autocomplete="current-password"
                required
            />
            <fieldset>
                <label for="remember">
                <input type="checkbox" role="switch" id="remember" name="remember" />
                Remember me
                </label>
            </fieldset>
            <button type="submit" class="contrast" onclick="event.preventDefault()">Login</button>
        </form>
        <br><hr><br>
        <center>
            <a href="{{ $page->baseUrl }}/en/post/how-to-raise-a-happy-kitten" role="button">EN</a>
            <a href="{{ $page->baseUrl }}/fr/post/comment-bien-elever-votre-chaton" role="button">FR</a>
    </center>
    </div>
</article>
@endsection
