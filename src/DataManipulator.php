<?php

class DataManipulator {
    private $data;

    public function __construct($initialData) {
        $this->data = $initialData;
    }

    public function add($value) {
        $this->data[] = $value;
    }

    public function remove($index) {
        $this->data = array_values(array_diff_key($this->data, [$index => true]));
    }

    public function sum() {
        return array_sum($this->data);
    }

    public function average() {
        $count = count($this->data);
        if ($count > 0) {
            return array_sum($this->data) / $count;
        } else {
            return 0;
        }
    }

    public function reverse() {
        $this->data = array_reverse($this->data);
    }

    public function filterEven() {
        $this->data = array_values(array_filter($this->data, function ($value) {
            return $value % 2 === 0;
        }));
    }

    public function getData() {
        return $this->data;
    }
}
