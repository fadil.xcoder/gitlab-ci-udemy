<?php

return [
    'production' => false,
    'baseUrl' => '',
    'title' => 'FX',
    'description' => 'Jigsaw v.%COMMIT%.',
    'author' => 'FADILXCODER',
    'path' => '{language}/{type}/{-title}',
    'collections' => [
            'posts-fr' => [
                'type' => 'post',
                'language' => 'fr',
            ],
            'posts-en' => [
                'type'  => 'post',
                'language' => 'en',
            ],
    ],
    'contact_email' => 'fadil@xcoder.dvlpr',
];
